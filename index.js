// console.log("Hello World!");


/*
	OBJECTS
		- An object is a data type that is used to represent real world objects. It is also a collection of related data and functionalities.

	Object Literals
		- one of the methods in creating objects.

		Syntax:
			let objectName = {
				keyA: valueA
				keyB: valueB

			};

*/
let cellphone = {
	name: "Nokia 3210", 
	manufactureDate: 1999
};

console.log("Result from creating object using Object Literals: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects Using a Constructor Function (Object Constructor)
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instance/copies of an object.

	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA;
			this.keyB = valueB
		};

		let variableName = new function ObjectName(valueA, valueB);
		console.log(variableName);
	
	=========================================
			- this is for invoking; it refers to the global object.
			- don't forget the "new" keyword when creating a new object.
*/
	// We use PascalCase for the Constructor Function Name.
	// template
	function Laptop (name, manufactureDate) {
		this.name = name
		this.manufactureDate = manufactureDate
	};

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using Constructor Function:");
console.log(laptop)

let myLaptop = new Laptop("Macbook Air", [2020, 2021]);
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result of creating object without the new keyword:");
console.log(oldLaptop);

// Creating empty objects as placeholder.
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

myComputer = {
	name: "Asus",
	manufactureDate: 2012
};

console.log(myComputer);

/*
	Mini Activity:
Create an object constructor function to produce 2 objects with 3 key-value pairs.
Log the 2 new objects in the console and send SS in our batch hangout.
*/

function Drink (name, flavor, type) {
	this.name = name
	this.flavor = flavor
	this.type = type
};

let drink1 = new Drink("Mountain Dew", "Lemon", "Soda")
let drink2 = new Drink("Tang","Orange","Powdered")
console.log(drink1)
console.log(drink2)

// Acessing Object Properties
	
// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name);

// Using the square bracket notation

console.log("Result from square bracket notation: " +myLaptop["name"]);

// Accessing array of objects
let deviceArr = [laptop, myLaptop];
// let deviceArr = [{name: Lenovo, manufactureDate: 2008}, {name: Macbook Air, manufactureDate: 2020}];

// Dot Notation
console.log(deviceArr[0].manufactureDate);

// Square Bracket Notation
console.log(deviceArr[0]["manufactureDate"]);

// Initializing/Adding/Deleting/Reassigning Object Properties
// (CRUD Operations)

// Initializing Object
let car = {};
console.log(car);

// Adding Object Properties
car.name = "Honda Civic";
console.log("Result from adding property using dot notation: ");
console.log(car);

car["manufacture date"] = 2019;
console.log("Result from adding property using square bracket notation: ");
console.log(car);

// Deleting object properties
delete car["manufacture date"];
console.log("Result from deleting object properties: ");
console.log(car);

// Reassigning object properties (update)
car.name = "Tesla";
console.log("Result from reassigning property: ");
console.log(car);

// Object Methods

/*
	This method is a function which is stored in an object property. They are also function and one of the key difference that they have is that method are functions related to a specific object.
*/

let person = {
	name: "John",
	age: 25,
	talk: function() {
		console.log("Hello! My name is " + this.name)
	}
};

console.log(person);
console.log("This is the result from Object Method: ");
person.talk();

person.walk = function() {
	console.log(this.name + " have walked 25 steps forward.")
}

person.walk();


let friend = {
	firstName: "Jane",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["janedoe@mail.com", "jane121992@gmail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName + "." + "I live in " + this.address.city + ", " + this.address.country + ".")
	}
};

friend.introduce();

// Real World Application of Objects

// Using an Object Literal

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 15,
	tackle: function () {
		console.log(this.name + " tackled targetPokemon")
		console.log("targetPokemon's heath is now reduced to targetPokemonHealth")
	},
	faint: function() {
		console.log(this.name + " fainted.")
	}
};

console.log(myPokemon);
myPokemon.tackle();

// Creating real world object using constructor function
/*function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 5 * level
	this.attack = 2 * level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
	},

	this.faint = function() {
		console.log(this.name + " fainted.")
	}
};

let charmander = new Pokemon("Charmander", 12);
let squirtle = new Pokemon("Squirtle", 6);

console.log(charmander);
console.log(squirtle);

charmander.tackle(squirtle);*/

// =============================

function Pokemon(name, level) {

	// Properties
	this.name = name
	this.level = level
	this.health = 5 * level
	this.attack = 2 * level

	// Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		
		/*console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))*/

		
	},

	this.anotherAttack = function(tackle2) {
		let strongTackle = tackle2.health-tackle2.health
		console.log(tackle2.name + " use tackle again.")
		console.log(this.name + "'s health is now reduced to " + strongTackle)
		console.log(this.name + "'s trainer used revive")
		let alive = this.health
		console.log(this.name + "'s health is now "+ alive)	
		console.log(tackle2.name + " use tackle again.")
		console.log(this.name + "'s health is now reduced to " + strongTackle + ". It was super effective.")
	}
};

let charmander = new Pokemon("Charmander", 12);
let squirtle = new Pokemon("Squirtle", 6);
let rattata = new Pokemon("Rattata", 14);
let meowth = new Pokemon("Meowth", 7);

console.log(rattata);
console.log(meowth);

rattata.tackle(meowth);
meowth.anotherAttack(rattata);